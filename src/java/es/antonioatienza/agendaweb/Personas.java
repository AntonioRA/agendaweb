/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agendaweb;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Antonio Rojo Atienza <antonioatienza.es>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Personas {

    @XmlElement(name = "persona")
    private List<Persona> listaPersona;

    public Personas() {
    }

    public List<Persona> getListaPersona() {
        return listaPersona;
    }

    public void setListaPersona(List<Persona> listaPersona) {
        this.listaPersona = listaPersona;
    }

}
