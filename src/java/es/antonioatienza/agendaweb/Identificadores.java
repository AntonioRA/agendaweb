/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agendaweb;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Antonio Atienza
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Identificadores {

    @XmlElement(name = "identificador")
    private List<Identificador> listaIdentificador;

    public Identificadores() {
    }

    public List<Identificador> getListaIdentificador() {
        return listaIdentificador;
    }

    public void setListaIdentificador(List<Identificador> listaIdentificador) {
        this.listaIdentificador = listaIdentificador;
    }
}
