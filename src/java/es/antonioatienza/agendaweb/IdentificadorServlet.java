/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agendaweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Antonio Atienza
 */
@WebServlet(name = "IdentificadorServlet", urlPatterns = {"/IdentificadorServlet"})
public class IdentificadorServlet extends HttpServlet {

    private byte tipoPeticion;
    public static final byte PETICION_GET_IDENTIFICADOR = 0; // Seleccionar
    private static final Logger LOG = Logger.getLogger(AgendaServlet.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws javax.xml.bind.JAXBException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JAXBException {
        response.setContentType("text/xml;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            try {
                EntityManager entityManager
                        = Persistence.createEntityManagerFactory("AgendaWebPU").createEntityManager();

                // Ejecutar una consulta (a generated named query in entity class)
                Query query = entityManager.createNamedQuery("Identificador.findByIdPersona");
                query.setParameter("idPersona", 1);

                Identificadores identificadores = new Identificadores();
                // Obtener una lista de objetos como resultado de la consulta previa
                identificadores.setListaIdentificador(query.getResultList());

                JAXBContext jaxbContext = JAXBContext.newInstance(Identificadores.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

                entityManager.getTransaction().begin();
                switch (tipoPeticion) {
                    case PETICION_GET_IDENTIFICADOR:
                        LOG.fine("Peticion Get");
                        // Convertir a XML el contenido de personas, generando el resultado en out
                        jaxbMarshaller.marshal(identificadores, out);
                        break;
                }
                entityManager.getTransaction().commit();

            } catch (JAXBException e) {
                out.println("<error>");
                e.printStackTrace(out);
                out.println("</error>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(IdentificadorServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(IdentificadorServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
