/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agendaweb;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Antonio Atienza
 */
@Entity
@Table(name = "identificador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Identificador.findAll", query = "SELECT i FROM Identificador i"),
    @NamedQuery(name = "Identificador.findById", query = "SELECT i FROM Identificador i WHERE i.id = :id"),
    @NamedQuery(name = "Identificador.findByActivado", query = "SELECT i FROM Identificador i WHERE i.activado = :activado"),
    @NamedQuery(name = "Identificador.findByIdPersona", query = "SELECT i FROM Identificador i WHERE i.idPersona = :idPersona")})
public class Identificador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Column(name = "codigo_identificacion")
    private byte[] codigoIdentificacion;
    @Column(name = "activado")
    private Boolean activado;
    @Column(name = "id_persona")
    private Integer idPersona;

    public Identificador() {
    }

    public Identificador(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getCodigoIdentificacion() {
        return codigoIdentificacion;
    }

    public void setCodigoIdentificacion(byte[] codigoIdentificacion) {
        this.codigoIdentificacion = codigoIdentificacion;
    }

    public Boolean getActivado() {
        return activado;
    }

    public void setActivado(Boolean activado) {
        this.activado = activado;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Identificador)) {
            return false;
        }
        Identificador other = (Identificador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.antonioatienza.agendaweb.Identificador[ id=" + id + " ]";
    }
    
}
