/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.antonioatienza.agendaweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Antonio Atienza
 */
@WebServlet(name = "AgendaServlet", urlPatterns = {"/AgendaServlet"})
public class AgendaServlet extends HttpServlet {

    public static final byte PETICION_GET = 0;    // Seleccionar
    public static final byte PETICION_POST = 1;   // Insertar
    public static final byte PETICION_PUT = 2;    // Actualizar
    public static final byte PETICION_DELETE = 3; // Borrar

    private byte tipoPeticion;
    private static final Logger LOG = Logger.getLogger(AgendaServlet.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws javax.xml.bind.JAXBException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JAXBException {
        response.setContentType("text/xml;charset=UTF-8");

        LOG.setLevel(Level.ALL);

        try (PrintWriter out = response.getWriter()) {
            try {
                EntityManager entityManager
                        = Persistence.createEntityManagerFactory("AgendaWebPU").createEntityManager();

                // Ejecutar una consulta (a generated named query in entity class)
                Query query = entityManager.createNamedQuery("Persona.findAll");

                Personas personas = new Personas();
                // Obtener una lista de objetos como resultado de la consulta previa
                personas.setListaPersona(query.getResultList());

                JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

                entityManager.getTransaction().begin();
                switch (tipoPeticion) {
                    case PETICION_GET:
                        LOG.fine("Peticion Get");
                        for (Persona persona : personas.getListaPersona()) {
                            // Añadir cada objeto a la lista general
                            LOG.fine("Lista personas[ Id=" + persona.getId()
                                    + "; Nombre=" + persona.getNombre() + " ]");
                        }
                        // Convertir a XML el contenido de personas, generando el resultado en out
                        jaxbMarshaller.marshal(personas, out);
                        break;
                    case PETICION_POST:
                        // Obtener la persona que se quiere insertar
                        Persona personaNueva = (Persona) jaxbUnmarshaller.unmarshal(request.getInputStream());
                        entityManager.persist(personaNueva);
                        LOG.fine("Peticion Post: Id=" + personaNueva.getId() + " Nombre=" + personaNueva.getNombre()
                                + " Fecha=" + personaNueva.getFecha());
                        break;
                    case PETICION_PUT:
                        // Obtener la persona que se quiere actualizar
                        Persona personaEditada = (Persona) jaxbUnmarshaller.unmarshal(request.getInputStream());
                        entityManager.merge(personaEditada);
                        LOG.fine("Peticion Put: Id=" + personaEditada.getId()+ " Nombre=" + personaEditada.getNombre());
                        break;
                    case PETICION_DELETE:
                        Persona personaEliminada = (Persona) jaxbUnmarshaller.unmarshal(request.getInputStream());
                        //Buscamos el id del objeto persona que queremos borrar en la base de datos
                        personaEliminada = entityManager.find(Persona.class, personaEliminada.getId());
                        entityManager.remove(personaEliminada);
                        LOG.fine("Peticion Delete: Id=" + personaEliminada.getId() + " Nombre=" + personaEliminada.getNombre());
                        break;
                    default:
                        break;
                }
                entityManager.getTransaction().commit();

            } catch (JAXBException e) {
                out.println("<error>");
                e.printStackTrace(out);
                out.println("</error>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_GET;
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(AgendaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_POST;
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(AgendaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        tipoPeticion = PETICION_DELETE;
        try {
            processRequest(req, resp);
        } catch (JAXBException ex) {
            Logger.getLogger(AgendaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        tipoPeticion = PETICION_PUT;
        try {
            processRequest(req, resp);
        } catch (JAXBException ex) {
            Logger.getLogger(AgendaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
